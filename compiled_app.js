var _dec, _desc, _value, _class, _class2;

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
	var desc = {};
	Object['ke' + 'ys'](descriptor).forEach(function (key) {
		desc[key] = descriptor[key];
	});
	desc.enumerable = !!desc.enumerable;
	desc.configurable = !!desc.configurable;

	if ('value' in desc || desc.initializer) {
		desc.writable = true;
	}

	desc = decorators.slice().reverse().reduce(function (desc, decorator) {
		return decorator(target, property, desc) || desc;
	}, desc);

	if (context && desc.initializer !== void 0) {
		desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
		desc.initializer = undefined;
	}

	if (desc.initializer === void 0) {
		Object['define' + 'Property'](target, property, desc);
		desc = null;
	}

	return desc;
}

function log(type) {
	return (target, property, descriptor) => {
		const oryginal = (descriptor || {}).value;
		switch (typeof oryginal) {
			case 'function':
				{

					descriptor.value = function () {
						console.log("LOG TYPE: ", type);
						console.log("FUNC NAME: ", property, "CLASS NAME: ", target.constructor.name);
						const result = oryginal.apply(this, arguments);
						console.log("LOG, result:", result);
					};
					break;
				}

			case 'xxx':
				{
					console.log("------------ 2 ------------");
					console.log("LOG", descriptor ? descriptor.value : "none");
					console.log("------------ 2 ------------");
					break;
				}
		}
	};
}

function classLog(classType) {
	return function (...args) {

		console.log('CLASSLOG', args);
		return new classType(...args);
	};
}

let Siemanko = (_dec = log('minimal'), (_class = class Siemanko {
	xx(a, b) {
		return a + b;
	}

	constructor(x = 1, y = 'siema') {
		this.x = x;
		this.y = y;
	}

}, (_applyDecoratedDescriptor(_class.prototype, "xx", [_dec], Object.getOwnPropertyDescriptor(_class.prototype, "xx"), _class.prototype)), _class));

let NoWitam = classLog(_class2 = class NoWitam {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

}) || _class2;

let siema = new Siemanko(),
    witam = new NoWitam(2, "no witam 2");
siema.xx(1, 3);
console.table(siema);
console.table(witam);
