function log(type) {
	return (target, property, descriptor) => {
		const oryginal = (descriptor || {}).value;
		switch(typeof oryginal){
			case 'function': {
				
				descriptor.value = function() {
					console.log("LOG TYPE: ", type);
					console.log("FUNC NAME: ", property, "CLASS NAME: ", target.constructor.name);
					const result = oryginal.apply(this, arguments);
					console.log("LOG, result:", result);
				}
				break;
			}

			case 'xxx': {
				console.log("------------ 2 ------------");
				console.log("LOG", descriptor ? descriptor.value :  "none");
				console.log("------------ 2 ------------");
				break;
			}
		}
	}
}

function classLog(classType) {
	return function(...args){
		
		console.log('CLASSLOG', args);
		return new classType(...args);
	
	}
}


class Siemanko{
	@log('minimal')
	xx(a, b) {
		return a + b;
	}

	constructor(x=1, y='siema'){
		this.x = x;
		this.y = y;
	}
	
}


@classLog
class NoWitam{
	constructor(x, y){
		this.x = x;
		this.y = y;
	}
	
}

let siema = new Siemanko(),
	witam = new NoWitam(2, "no witam 2");
siema.xx(1,3);
console.table(siema);
console.table(witam);